/************************************************************************
* Copyright © Joao Ferreira                                             *
*                                                                       *
* This program is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or     *
*  (at your option) any later version.                                  *
*                                                                       *
* This program is distributed in the hope that it will be useful,       *
* but WITHOUT ANY WARRANTY; without even the implied warranty of        *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
* GNU General Public License for more details.                          *
*                                                                       *
* You should have received a copy of the GNU General Public License     *
* along with this program.  If not, see <https://www.gnu.org/licenses/>.*
*                                                                       *
************************************************************************/


// PORT REGISTERS MAPPING

#ifndef	_PORT_SFR_
#define	_PORT_SFR_

__sbit	__at	(0x80)	P0_0;
__sbit	__at	(0x81)	P0_1;
__sbit	__at	(0x82)	P0_2;
__sbit	__at	(0x83)	P0_3;
__sbit	__at	(0x84)	P0_4;
__sbit	__at	(0x85)	P0_5;
__sbit	__at	(0x86)	P0_6;
__sbit	__at	(0x87)	P0_7;

__sbit	__at	(0x90)	P1_0;
__sbit	__at	(0x91)	P1_1;
__sbit	__at	(0x92)	P1_2;
__sbit	__at	(0x93)	P1_3;
__sbit	__at	(0x94)	P1_4;
__sbit	__at	(0x95)	P1_5;
__sbit	__at	(0x96)	P1_6;
__sbit	__at	(0x97)	P1_7;

__sbit	__at	(0xA0)	P2_0;
__sbit	__at	(0xA1)	P2_1;
__sbit	__at	(0xA2)	P2_2;
__sbit	__at	(0xA3)	P2_3;
__sbit	__at	(0xA4)	P2_4;
__sbit	__at	(0xA5)	P2_5;
__sbit	__at	(0xA6)	P2_6;
__sbit	__at	(0xA7)	P2_7;

__sbit	__at	(0xB0)	P3_0;
__sbit	__at	(0xB1)	P3_1;
__sbit	__at	(0xB2)	P3_2;
__sbit	__at	(0xB3)	P3_3;
__sbit	__at	(0xB4)	P3_4;
__sbit	__at	(0xB5)	P3_5;
__sbit	__at	(0xB6)	P3_6;
__sbit	__at	(0xB7)	P3_7;

#endif