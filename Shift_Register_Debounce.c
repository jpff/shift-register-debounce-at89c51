/************************************************************************
* Copyright © Joao Ferreira                                             *
*                                                                       *
* This program is free software: you can redistribute it and/or modify  *
* it under the terms of the GNU General Public License as published by  *
* the Free Software Foundation, either version 3 of the License, or     *
*  (at your option) any later version.                                  *
*                                                                       *
* This program is distributed in the hope that it will be useful,       *
* but WITHOUT ANY WARRANTY; without even the implied warranty of        *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
* GNU General Public License for more details.                          *
*                                                                       *
* You should have received a copy of the GNU General Public License     *
* along with this program.  If not, see <https://www.gnu.org/licenses/>.*
*                                                                       *
************************************************************************/


#include <at89x51.h>
#include <stdint.h>	// Where typedef integer types like uint8_t are defined
#include "port_sfr.h"

#define	BUTTON	P3_3
#define	LED	P1_0

#define	TIMER0VAL_uS		10000						// Pin polling takes place every 10mS
#define	TIMER0VAL_OVERFLOW	(0xFFFF - TIMER0VAL_uS)				// Timer 0 16bit overflow value

#define HYSTERESIS_VAL		10						// Can be from 0 to 255, if this program's stock' 8 bit shift register is kept

// By initializing timer values as constants, instead of using the pre-processor's targeted #define macro,
// we -make sure- the compiler calculates the values during compilation-time, instead of replacing the expression
// with the pre-defined instructions, saving on unnecessary & valuable runtime CPU operations.
const uint8_t c_timer0val_TL0 = (uint8_t)(TIMER0VAL_OVERFLOW & 0x00FF);		// Timer 0 LSB value, cast to uint8_t
const uint8_t c_timer0val_TH0 = (uint8_t)((TIMER0VAL_OVERFLOW >> 8) & 0x00FF);	// Timer 0 MSB value, cast to uint8_t

volatile uint8_t g_debouncedPin = 0;						// Holds the state of the debounced pin value
volatile uint8_t g_debounceShiftRegister = 0x00;				// 8 bit shift register

const uint8_t c_hysteresis_LowVal = (uint8_t)HYSTERESIS_VAL;
const uint8_t c_hysteresis_HighVal = (uint8_t)(sizeof(g_debounceShiftRegister) - HYSTERESIS_VAL);

void setup();
void loop();

void main(void)
{
	setup();
	while(1)	loop();
}

void setup()
{
	// Load Timer 0 values to overflow every 10000uS or 10mS, TL0 = 0xF0, TH0 = 0xD8
	TL0 = c_timer0val_TL0;
	TH0 = c_timer0val_TH0;

	// Configure Timer 0 as 16 bit, mode 1, osc. at aprox. 12MHz, setting all the appropriate Special Function Register flags
	ET0 = 1;	// Enable Timer 0 interruption
	TMOD |= 0x01;	// Set Timer 0 as 16 bit/mode 1
	EA = 1;		// Make sure the master interrupt control flag is set
	TR0 = 1;	// Start Timer 0
}

void loop()
{
	// Turn LED on port 3, pin 3, according to the state of the virtual debounced pin BUTTON
	(g_debouncedPin == 1) ? (LED = 1) : (LED = 0);
}

void TIMER0_ISR (void)	__interrupt 1
{
	TL0 = c_timer0val_TL0;	// Reload TL0 LSB value
	TH0 = c_timer0val_TH0;	// Reload TH0 MSB value

	// Shift Register Debounce routine
	g_debounceShiftRegister <<= 1;			// Shift the register to the left by 1 unit
	g_debounceShiftRegister &= 0xFE;		// Clear LSB
	g_debounceShiftRegister |= BUTTON;		// Read raw pin value

	// Then ,set the virtual debounced pin accordingly
	// Without hysteresis
	//(g_debounceShiftRegister == 0xFF) ? (g_debouncedPin = 1) : (g_debouncedPin = 0);


	// With hysteresis, comment-out previous line) set the virtual debounced pin accordingly
	(g_debouncedPin == 0) ? ((g_debounceShiftRegister > c_hysteresis_HighVal) ? (g_debouncedPin = 1) : 1) : 1;
	(g_debouncedPin == 1) ? ((g_debounceShiftRegister < c_hysteresis_LowVal) ? (g_debouncedPin = 0) : 1) : 1;
}